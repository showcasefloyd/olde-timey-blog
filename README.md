# olde-timey-blog

To run this you will need to create a db_credentials.php file in the root of the /Private directory.

The file should be set up like so.

    // Database Credentials
    define('DBNAME','fill in your name');
    define('DBSERVER','fill in your url');
    define('DBUSER','fill in your username');
    define('DBPASS','fill in your password');

## TO DO

- Convert from mysqli to PDO
