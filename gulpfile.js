const browserSync = require("browser-sync");
const gulp = require("gulp");
const phpConnect = require('gulp-connect-php');
const sass = require("gulp-sass");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");
const sourcemaps = require("gulp-sourcemaps");

var paths = {
  styles: {
    src: "src/scss/**/*.scss",
    dest: "dist/public/css"
  }
};

function style() {
  return gulp
    .src(paths.styles.src)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on("error", sass.logError)
    .pipe(postcss([autoprefixer()]))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(browserSync.stream());
}

// PHP connect
function connectsync() {
  phpConnect.server({
    // a standalone PHP server that browserSync connects to via proxy
    port: 8000,
    keepalive: true,
    base: "dist/public"
  },
    function () {
      browserSync({
        proxy: '127.0.0.1:8000'
      });
    });
}

// BrowserSync Reload
function browserSyncReload(done) {
  browserSync.reload();
  done();
}

function php() {
  return gulp.src("./dist/**/*.php")
}

// Watch files
function watchFiles() {
  gulp.watch(["dist/**/*.php", paths.styles.src], gulp.series(style, php, browserSyncReload));
}

const watch = gulp.parallel([watchFiles, connectsync]);

exports.style = style;
exports.default = watch;