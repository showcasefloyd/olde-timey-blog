<?php 
require_once('../private/setup.php'); 


if($_GET['id'] != NULL){
  $post = Posts::find_by_id($_GET['id']);
} else {
  redirect('admin.php');
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){

  if(isset($_POST['cancel'])){
    redirect('admin.php');
  }

  $args = [];
  $args = $_POST['post'] ;

  $post->updated_attributes($args);
  $post->update();
  redirect('admin.php');
}
?>

<?php $page_title = "Ye olde timey blog : Edit Post"; ?>
<?php include_once(SHARED_PATH .'/header.php'); ?>
  
<div class="row">
  <div class="col-sm-12 ">
    <div class="blog-header card fluid">
      <h2><?php echo $page_title; ?></h2>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12 ">

  <form action="edit_blog.php?id=<?php echo $post->id; ?>" method="post">
    <fieldset>
      <legend>Edit Post</legend>
      <?  include_once(SHARED_PATH .'/form_include.php');  ?>
      <button name="save" class="btn">Save</button> <button name="cancel" class="btn">Cancel</button>
    </fieldset>
  </form>  
  </div> 
</div>

<?php include_once(SHARED_PATH .'/footer.php'); ?>