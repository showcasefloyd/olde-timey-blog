<?php require_once('../private/setup.php'); ?>
<?php $page_title = "Ye olde timey blog"; ?>
<?php $id = $_GET['id'] ?? NULL; ?>
<?php include_once(SHARED_PATH .'/header.php'); ?>
<?php
  $post = Posts::find_by_id($id);
?>

<div class="row">
  <div class="col-sm-12 ">
    <div class="blog-header card fluid">
      <h2><?php echo $page_title; ?></h2>
    </div>
  </div>
</div>

  <div class="row">
    <div class="col-sm-12 col-md-9">

      <div class="blog-post card fluid">
        
        <h2>
          <?php echo $post->title; ?>
          <small><?php echo $post->description; ?></small>
        </h2>
        
        <div class="blog-details">
          <p>Posted on: <i><?php echo $post->date(); ?></i></p>
          <p>By: </i><?php echo $post->author(); ?></i></p>
        </div>
       
        <hr>

        <div class="blog">
          <?php echo htmlentities($post->blog_post); ?>
        </div>
         
        <div class="category-tags">
          File under: <mark class="tag"><?php echo $post->category(); ?></mark>
        </div>

      </div>
    </div>

    <?php include_once(SHARED_PATH .'/rightcolumn-menu.php'); ?>

  </div>

<?php include_once(SHARED_PATH .'/footer.php'); ?>