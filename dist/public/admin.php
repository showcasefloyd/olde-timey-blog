<?php require_once('../private/setup.php'); ?>
<?php $page_title = "Ye olde timey blog : Admin"; ?>
<?php include_once(SHARED_PATH .'/header.php'); ?>

<?php 
$action = $_GET['action'] ?? NULL;

if($_SERVER['REQUEST_METHOD'] == 'GET'){
  
  if($action == 'delete'){
    $post = Posts::find_by_id($_GET['id']);
    $post->delete();
    redirect('admin.php');
  }  

  $column = $_GET['column'] ?? 'id';
  $order = $_GET['order'] ?? 'desc';
  
  // Set the default table sort order
  $sort_order = ($order == 'desc') ? 'DESC' : 'ASC';
  $add_class = (isset($_GET['order'])) ? ' class="highlight"' : '';
  

  $up_or_down = $sort_order == 'ASC' ? "&uarr;" : "	&darr;"; 
  $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';

}
?>

<div class="row">
  <div class="col-sm-12 ">
    <div class="blog-header card fluid">
      <h2><?php echo $page_title; ?></h2>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12 col-md-9">

  <table class="admin-table">
    <thead>
      <tr>
        <th <?php echo $column == 'id' ? $add_class : ''; ?> ><a href="admin.php?column=id&order=<?php echo $asc_or_desc; ?>"> Id <?php echo ($column == 'id') ? $up_or_down : ''; ?></a> </th>
        <th <?php echo $column == 'title' ? $add_class : ''; ?> ><a href="admin.php?column=title&order=<?php echo $asc_or_desc; ?>"> Title <?php echo ($column == 'title') ? $up_or_down : ''; ?></a></th>
        <th <?php echo $column == 'author_id' ? $add_class : ''; ?> ><a href="admin.php?column=author_id&order=<?php echo $asc_or_desc; ?>"> Author <?php echo ($column == 'author_id') ? $up_or_down : ''; ?></a></th>
        <th <?php echo $column == 'update_time' ? $add_class : ''; ?> ><a href="admin.php?column=update_time&order=<?php echo $asc_or_desc; ?>"> Published <?php echo ($column == 'update_time') ? $up_or_down : ''; ?></a></th>
        <th <?php echo $column == 'category_id' ? $add_class : ''; ?> ><a href="admin.php?column=category_id&order=<?php echo $asc_or_desc; ?>"> Category <?php echo ($column == 'category_id') ? $up_or_down : ''; ?></a></th>
        <th>View</div>
        <th>Edit</div>
        <th>Delete</div>
      </tr>
    </thead>
    <tbody>
      <?php
          $posts = Posts::find_all($column,$sort_order);
          foreach($posts as $post) {
            ?>
            <tr>
              <td><?php echo $post->id; ?></td>
              <td><?php echo $post->title; ?></td>
              <td><?php echo "[ ". $post->author_id ." ] ". $post->author() ?></td>
              <td><?php echo $post->date(); ?></td>
              <td><?php echo "[ ". $post->category_id ." ] ".$post->category(); ?></td>
              <td><a href="view_blog.php?id=<?php echo $post->id; ?>">View</a></td>
              <td><a href="edit_blog.php?id=<?php echo $post->id; ?>">Edit</a></td>
              <td><a href="admin.php?id=<?php echo $post->id; ?>&action=delete">Delete</a></td>
            </tr>
            
        <?php } ?>
    </tbody>
  </table>
  
  </div>   

  <?php include_once(SHARED_PATH .'/rightcolumn-menu.php'); ?>

</div>

<?php include_once(SHARED_PATH .'/footer.php'); ?>