<?php require_once('../private/setup.php'); ?>
<?php $page_title = "Ye olde timey blog"; ?>
<?php include_once(SHARED_PATH .'/header.php'); ?>

<div class="row">
  <div class="col-sm-12 ">
    <div class="blog-header card fluid">
      <h2><?php echo $page_title; ?></h2>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12 col-md-9">

    <?php
      $posts = Posts::find_all();
      foreach($posts as $post) {
    ?>

      <div class="blog-card card fluid">

        <h3>
          <?php echo "<a href='view_blog.php?id=".$post->id."'>".$post->title ."</a>"; ?>
          <small><?php echo $post->description; ?></small>
        </h3>
        
        <div class="blog-card-listing">
          <p>
            Posted: <i><?php echo $post->date() ?> </i><br>
            Author: <i><?php echo $post->author() ?></i><br>
            File under: <mark class="tag"><?php echo $post->category(); ?></mark>
          </p>
          <p><a href="view_blog.php?id=<?php echo $post->id; ?>">View</a></p>
        </div>
        
      </div>

    <?php } ?>
 
  </div>   

  <?php include_once(SHARED_PATH .'/rightcolumn-menu.php'); ?>
</div>

<?php include_once(SHARED_PATH .'/footer.php'); ?>