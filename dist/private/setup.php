<?php
ob_start();

// Define file paths to PHP constants 
define('PRIVATE_PATH', dirname(__FILE__));
define('PROJECT_PATH', dirname(PRIVATE_PATH));
define('PUBLIC_PATH', PROJECT_PATH .'/public');
define('SHARED_PATH', PRIVATE_PATH .'/includes');

date_default_timezone_set("America/New_York");

require_once('db_credentials.php');
require_once('db_functions.php');
require_once('functions.php');

// Autoload class files
function my_autoload($class){
  if(preg_match('/\A\w+\Z/',$class)){
    require_once('classes/'. $class .'.class.php');
  }
}

spl_autoload_register('my_autoload');

// Static method
$database = db_connect();
Database::set_database($database);