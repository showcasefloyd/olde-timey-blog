<?php

function db_connect(){
  $dbconn = new mysqli(DBSERVER, DBUSER, DBPASS, DBNAME);
  verify_connection($dbconn);
  return $dbconn;
}

function verify_connection($dbconn){
  if($dbconn->connect_errno){
    $errMsg  = 'Database connection failed: ';
    $errMsg .= $dbconn->connect_error;
    $errMsg .= " (". $dbconn->connect_errno .") ";

    exit('Connection failed: '. $errMsg);
  }
}

function close_connection($dbconn){
  if(isset($dbconn)){
    $dbconn->close();
  }
}