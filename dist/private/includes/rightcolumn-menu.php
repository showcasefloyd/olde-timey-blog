<div class="col-sm-12 col-md-3">
    <div class="blog-card card fluid">
      <h2>About Me</h2>
      <p>I'm mild manner reporter living in a large Metropolitan city. I like PHP, Comic Books, Skateboards and Music</p>
    </div>
  
    <div class="blog-card card fluid">
        <h3>Links</h3>
        <ul>        
          <li><a href="./index.php">Home</a></li>
          <?php if($_SERVER['REQUEST_URI'] === '/admin.php'){ ?>
            <li><a href="./add_blog.php">Add New Post</a></li>
          <?php } else { ?>
            <li><a href="./admin.php">Log In</a></li>
          <?php } ?>
        </ul>
    </div>
    
    <div class="blog-card card fluid">
      <h3>Categories</h3>
      <ul>
        <?php 
          $categories = Posts::categories();
          foreach($categories as $category){
            print "<li>". $category ."</li>";
          }
        ?>
      </ul>
    </div>

    <div class="blog-card card fluid">
      <h3>Follow Me</h3>
      <p>Not on Facebook, not on Twitter and not on Instagram.</p>
    </div>
  </div>
