

    <div class="row">
      <div class="col-sm-12 col-md-3">
        <label for="Title">Title:</label> 
      </div>
      <div class="col-sm-12 col-md">
        <input style="width:85%" id="title" name="post[title]" type="text" placeholder="Title" value="<?php echo htmlspecialchars($post->title); ?>">
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 col-md-3">
        <label for="Date">Date:</label> 
      </div>
      <div class="col-sm-12 col-md">
        <input style="width:85%" id="date" name="date" type="text" value="<?php echo $post->date(); ?>" disabled>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 col-md-3">
        <label for="Author">Author:</label> 
      </div>
      <div class="col-sm-12 col-md">
        <select name="post[author_id]" style="width:45%;" >
        <option value=""> Select Author </option>
        <?php
          $authors = Users::find_all();
          foreach($authors as $author){
            $select = ($author->id === $post->author_id) ? 'selected' : '';
            echo "<option value=". $author->id ." ". $select .">". $author->first ." ". $author->last ."</option>";
          }
        ?>  
        </select>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 col-md-3">
        <label for="Category">Category:</label> 
      </div>
      <div class="col-sm-12 col-md">
        <select name="post[category_id]" style="width:45%;" >
        <option value=""> Select Category </option>
        <?php 
          $categories = Posts::categories();
          foreach($categories as $key => $value){
            $select = ($key == $post->category_id) ? 'selected' : '';
            echo "<option value=". $key ." ". $select .">". $value ."</option>";
          }
        ?>
        </select>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 col-md-3">
        <label for="Description">Description:</label> 
      </div>
      <div class="col-sm-12 col-md">
        <input style="width:85%" id="description" name="post[description]" type="text" placeholder="Description" value="<?php echo htmlspecialchars($post->description); ?>">
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 col-md-3">
        <label for="Post">Blog Post</label>
      </div>  
      <div class="col-sm-12 col-md">
        <textarea rows="8" style="width:85%;" id="blog_post" name="post[blog_post]" class="form-element"><?php echo htmlspecialchars($post->blog_post); ?></textarea>
      </div>
    </div>