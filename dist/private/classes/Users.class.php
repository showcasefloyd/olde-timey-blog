<?php

class Users extends Database {

  static protected $table_name = 'users';
  
  public $id; // Added by database
  public $first;
  public $last;
  public $email;
  public $type;
  public $create_time;
  public $update_time;

  public function __construct($args = []){

    $this->first = $args['first'] ?? NULL;
    $this->last = $args['last'] ?? NULL;
    $this->email = $args['email'] ?? NULL;
    $this->type = $args['type'] ?? NULL;

  }

  static public function find_all(){
    $sql = "SELECT id,first,last,email FROM ". self::$table_name;
    $sql .= " WHERE type = 'author'";

    return parent::find_by_sql($sql);
  }

}