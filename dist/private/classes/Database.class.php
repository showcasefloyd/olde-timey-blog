<?php

class Database {

  // All instances share the same db connection
  static protected $database;
  static protected $table_name;

  static public function set_database($database){
    self::$database = $database;
  }

  static public function find_by_sql($sql){
    
    $result = self::$database->query($sql);

    if(!$result){
      exit("Database query failed: " . $sql);
    }

    // Turn our result into an object of calling class;
    $object_array = [];
    while($row = $result->fetch_assoc()){
      $object_array[] = self::instantiate($row);
    }    
    $result->free(); 
    return $object_array;
  }

  static protected function instantiate($record){
    
    // Refers to any class that wants to instantiate it self
    // Static is an example of late binding
    $obj = new static;

    foreach($record as $property => $value){
      if(property_exists($obj, $property)){
        $obj->$property = $value;
      }
    }

    return $obj;
  }

}