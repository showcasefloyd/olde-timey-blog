<?php

class Posts extends Database {

  static public function find_all($col = 'id',  $sortOrder = 'DESC'){
    $sql = 'SELECT p.*, u.first,u.last,u.email FROM posts p ';
    $sql .= 'INNER JOIN users u ON p.author_id = u.id ';
    $sql .= 'ORDER BY '. $col .' '. $sortOrder ;
    return parent::find_by_sql($sql);
  }

  static public function find_by_id($id){
    $sql = 'SELECT p.*, u.first,u.last,u.email FROM posts p ';
    $sql .= 'INNER JOIN users u ON p.author_id = u.id ';
    $sql .= 'WHERE p.id = "'. $id .'" ';
    $record = parent::find_by_sql($sql);

    if(!empty($record)){
      return array_shift($record);
    } else {
      return false;
    }
  }

  // Class instance properties and methods

  const CATEGORIES = [
    1 => 'Web Development',
    2 => 'Rants',
    3 => 'Music',
    4 => 'Film',
    5 => 'Comic Books',
    6 => 'News'
  ];

  static protected $table_name = 'posts';
  
  public $id; // Added by database
  public $title;
  public $description;
  public $blog_post;
  public $author_id;
  public $category_id;
  public $create_time;
  public $update_time;
  
  protected $first;
  protected $last;
  protected $email;
  
  public static function categories(){
    return self::CATEGORIES;
  }

  public function __construct($args = []) 
  {
    $this->title = $args['title'] ?? NULL;
    $this->description = $args['description'] ?? NULL;
    $this->blog_post = $args['blog_post'] ?? NULL;
    $this->author_id = $args['author_id'] ?? NULL;
    $this->category_id = $args['category_id'] ?? NULL;
    $this->update_time = $args['update_time'] ?? date('M jS Y @ h:i a', time());;

  }

  public function category(){
    if($this->category_id > 0){
      return self::CATEGORIES[$this->category_id];
    } else {
      return 'No category set';
    }  
  }

  public function author(){
    if($this->author_id > 0){
      return $this->first . ' ' . $this->last .', '. $this->email;
    } else {
      return 'No author set';
    }
  }

  public function date(){
    return date('M jS Y @ h:i a', strtotime($this->update_time));
  }

  public function save(){

    $sql = "INSERT INTO ". self::$table_name  ." ( ";
    $sql .= " title, description, category_id, author_id, blog_post ";
    $sql .= ") VALUES ( ";
    $sql .= "'". self::$database->escape_string($this->title) ."', ";
    $sql .= "'". self::$database->escape_string($this->description) ."', ";
    $sql .= "'". self::$database->escape_string($this->category_id) ."', ";
    $sql .= "'". self::$database->escape_string($this->author_id) ."', ";
    $sql .= "'". self::$database->escape_string($this->blog_post) ."' ";
    $sql .= ")";

    $result = self::$database->query($sql); 
    if(!$result){
      exit("Insert query failed: " . $sql);
    } 
    $this->id = self::$database->insert_id;

    return $result;
  }

  public function update(){

    $sql = "UPDATE ". self::$table_name  ." SET ";
    $sql .= "title='". self::$database->escape_string($this->title) ."', ";
    $sql .= "description='". self::$database->escape_string($this->description) ."', ";
    $sql .= "category_id='". self::$database->escape_string($this->category_id) ."', ";
    $sql .= "author_id='". self::$database->escape_string($this->author_id) ."', ";
    $sql .= "blog_post='". self::$database->escape_string($this->blog_post) ."' ";
    $sql .= " WHERE id ='" . self::$database->escape_string($this->id) ."' ";
    $sql .= " LIMIT 1";

    $result = self::$database->query($sql); 
    if(!$result){
      exit("Update query failed: " . $sql);
    } 

    return $result;

  }

  public function updated_attributes($args=[]){
    foreach($args as $key => $value){
      if(property_exists($this, $key) && !is_null($value)){
        $this->$key = $value;
      }
    }

  }
  public function delete() {
    $sql = "DELETE from ". self::$table_name ." WHERE id='". $this->id ."' LIMIT 1";

    $result = parent::$database->query($sql);
    if(!$result){
      echo "Error: Record was not deleted: ". $sql;
    }

  }


}



